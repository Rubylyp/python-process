'''
Description: 绘图库
Version: 1.0
Author: Luoyanping
Date: 2022-07-13 20:50:20
LastEditors: Luoyanping
LastEditTime: 2023-09-19 17:07:51
'''
# 绘图库
import numpy as np
from copy import copy
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.path as mpath
import matplotlib.dates as mdate
import cartopy.crs as ccrs
import cartopy.feature as cfeat
from cartopy.mpl.ticker import LongitudeFormatter,LatitudeFormatter
from cartopy.mpl.gridliner import LATITUDE_FORMATTER, LONGITUDE_FORMATTER
from cartopy.io.shapereader import Reader, natural_earth
import shapely.geometry as sgeom


# # 中文设置为宋体 英文为极为接近的Times new roman
# config = {
#             "font.family": 'serif',
#             "font.size": 10,
#             "mathtext.fontset": 'stix',
#             "font.serif": ['SimSun'],
#          }
# plt.rcParams.update(config)

# 英文字体设置为罗马体
plt.rcParams['font.family'] = 'Times New Roman'
plt.rcParams['axes.unicode_minus'] =False


'''
description: 创建labmert的刻度
return {*}
author: Luoyanping
'''
# 创建labmert的刻度
def find_side(ls, side):
    """
    Given a shapely LineString which is assumed to be rectangular, return the
    line corresponding to a given side of the rectangle.
    
    """
    minx, miny, maxx, maxy = ls.bounds
    points = {
        'left': [(minx, miny), (minx, maxy)],
        'right': [(maxx, miny), (maxx, maxy)],
        'bottom': [(minx, miny), (maxx, miny)],
        'top': [(minx, maxy), (maxx, maxy)],}
    return sgeom.LineString(points[side])

def _lambert_ticks(ax, ticks, tick_location, line_constructor, tick_extractor):
    """Get the tick locations and labels for an axis of a Lambert Conformal projection."""
    outline_patch = sgeom.LineString(ax.outline_patch.get_path().vertices.tolist())
    axis = find_side(outline_patch, tick_location)
    n_steps = 30
    extent = ax.get_extent(ccrs.PlateCarree())
    _ticks = []
    for t in ticks:
        xy = line_constructor(t, n_steps, extent)
        proj_xyz = ax.projection.transform_points(ccrs.Geodetic(), xy[:, 0], xy[:, 1])
        xyt = proj_xyz[..., :2]
        ls = sgeom.LineString(xyt.tolist())
        locs = axis.intersection(ls)
        tick = [None] if not locs else tick_extractor(locs.xy)
        _ticks.append(tick[0])
    # Remove ticks that aren't visible:    
    ticklabels = copy(ticks)
    while True:
        try:
            index = _ticks.index(None)
        except ValueError:
            break
        _ticks.pop(index)
        ticklabels.pop(index)
    return _ticks, ticklabels

def lambert_xticks(ax, ticks):
    """Draw ticks on the bottom x-axis of a Lambert Conformal projection."""
    te = lambda xy: xy[0]
    lc = lambda t, n, b: np.vstack((np.zeros(n) + t, np.linspace(b[2], b[3], n))).T
    xticks, xticklabels = _lambert_ticks(ax, ticks, 'bottom', lc, te)
    ax.xaxis.tick_bottom()
    ax.set_xticks(xticks)
    ax.set_xticklabels([ax.xaxis.get_major_formatter()(xtick) for xtick in xticklabels])

def lambert_yticks(ax, ticks):
    """Draw ricks on the left y-axis of a Lamber Conformal projection."""
    te = lambda xy: xy[1]
    lc = lambda t, n, b: np.vstack((np.linspace(b[0], b[1], n), np.zeros(n) + t)).T
    yticks, yticklabels = _lambert_ticks(ax, ticks, 'left', lc, te)
    ax.yaxis.tick_left()
    ax.set_yticks(yticks)
    ax.set_yticklabels([ax.yaxis.get_major_formatter()(ytick) for ytick in yticklabels])

# 创建图像层
def create_fig(flag=None):
    if flag is None:
        fig = plt.figure(figsize=(8, 5), dpi=400)  # 创建页面
    else:
        fig = plt.figure(figsize=(8,5), dpi=400, frameon=True)
    return fig

# 创建坐标轴的三种方式 
# fig不会立即显示 plt会立即显示 多个坐标轴没研究过如何设置信息 推荐plt.subplot或fig.add_subplot
# 创建单个坐标轴
# ax = fig.add_axes([0.1, 0.1, 0.8, 0.8],projection=proj)
# ax = fig.subplot(1, 1, subplot_kw={'projection': proj})
# ax = fig.add_subplot(1,1,1)
# ax = plt.subplot(1,1,1, projection=proj)
# 创建多个坐标轴
# ax = plt.subplots(2,3)

# 创建Map类坐标轴-中国地区 LambertConformal
def create_map_Lambert(fig):
    # --创建地图投影
    proj = ccrs.LambertConformal(central_longitude=105, standard_parallels=(25, 47))

    # --创建画图空间
    ax = fig.add_subplot(1,1, 1, projection=proj)
    ax.set_extent([78, 128, 15, 55], crs=ccrs.PlateCarree())

    # --设置地图信息
    ## 加载分辨率为50的海岸线
    ax.add_feature(cfeat.COASTLINE.with_scale('50m'),linewidth=0.5,zorder=2,color='k')# 添加海岸线
    ## 加载分辨率为50的河流~
    ax.add_feature(cfeat.RIVERS.with_scale('50m'))
    ## 加载分辨率为50的湖泊~
    ax.add_feature(cfeat.LAKES.with_scale('50m'))  
    ## 加载分辨率为50的海洋
    ax.add_feature(cfeat.OCEAN.with_scale('50m'))
    ## 加载分辨率为50的陆地
    ax.add_feature(cfeat.LAND.with_scale('50m'))

    # --设置网格线属性
    # *must* call draw in order to get the axis boundary used to add ticks:
    fig.canvas.draw()
    # Define gridline locations and draw the lines using cartopy's built-in gridliner:
    xticks=[70,80,90,100,110,120]
    yticks=[15,25,35,45,55]
    
    ax.gridlines(
        xlocs=xticks, 
        ylocs=yticks, 
        draw_labels=False, 
        linewidth=0.8, 
        color='k', 
        alpha=0.6, 
        linestyle='--',
        )
    
    # Label the end-points of the gridlines using the custom tick makers:
    ax.xaxis.set_major_formatter(LONGITUDE_FORMATTER) 
    ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)
    # lambert_xticks(ax, xticks)
    # lambert_yticks(ax, yticks)

    font3={'family':'SimSun','size':10,'color':'k'}
    ax.set_ylabel("纬度",fontdict=font3)
    ax.set_xlabel("经度",fontdict=font3)

    # --设置刻度
    ax.tick_params(
        axis='both', 
        which='major',
        direction='out',
        length=3.5,
        width=0.8,
        color='k',
        pad=1,
        labelsize=6,
        labelcolor='k',
        top=False,
        right=False,
        labeltop=False,
        labelright=False,
        )
    ax.tick_params(
        axis='both', 
        which='minor',
        direction='out',
        length=2.5,
        width=0.8,
        color='k',
        pad=1,
        labelsize=5,
        labelcolor='k',
        top=False,
        right=False,
        labeltop=False,
        labelright=False,
        )

    ax.set_title('Lambert Conformal Ticks')
    return ax   
    
# 创建Map类坐标轴-东亚地区 PlateCarree
def create_map_PlateCarree(fig,scale=None,shp_file=None):
    # --创建地图投影
    proj = ccrs.PlateCarree()
    
    # --创建画图空间
    ax = fig.add_subplot(1,1, 1, projection=proj)

    # -- 设置范围
    ax.set_extent([70, 140, 10, 50], crs=ccrs.PlateCarree())
    
    # --设置地图信息
    if scale is None:
        scale = 50
    if shp_file is not None:
        provinces = cfeat.ShapelyFeature(
            Reader(shp_file).geometries(),
            proj, edgecolor='k',
            facecolor='none'
        )
        # 加载省界线
        ax.add_feature(provinces, linewidth=0.6, zorder=2)
    ## 加载分辨率为50的海岸线
    ax.add_feature(cfeat.COASTLINE.with_scale(f'{scale}m'), linewidth=0.6, zorder=10)
    ## 加载分辨率为50的河流~
    ax.add_feature(cfeat.RIVERS.with_scale(f'{scale}m'), zorder=10)
    ## 加载分辨率为50的湖泊~
    ax.add_feature(cfeat.LAKES.with_scale(f'{scale}m'), zorder=10)

    # *must* call draw in order to get the axis boundary used to add ticks:
    fig.canvas.draw()

    xticks = (np.arange(75, 135 + 5, 5)).tolist()
    yticks = (np.arange(10, 50 + 5, 5)).tolist()
    # --设置网格属性 Grindline+NCL风格
    gl = ax.gridlines(
        xlocs= xticks,
        ylocs= yticks,
        crs = ccrs.PlateCarree(),
        draw_labels = False,
        linewidth = 0.7,
        color = 'grey',
        alpha = 0.5,
        linestyle = '--'
    )

    # # --设置经纬度（1）经纬度是正的 与方法（3）类似，但经过了投影转换
    # ax.xaxis.set_major_formatter(LONGITUDE_FORMATTER) 
    # ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)
    # lambert_xticks(ax, xticks)
    # lambert_yticks(ax, yticks)

    # --设置经纬度（2）经纬度是斜着的 跟随gridline网格
    gl.top_labels=gl.right_labels=False   #关闭右和上经纬度
    gl.bottom_labels=gl.left_labels=True  #打开左和下经纬度
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlocator=mticker.FixedLocator(xticks)
    gl.ylocator=mticker.FixedLocator(yticks)
    
    # # --设置经纬度（3）只适用于等经纬度 未经过投影转换
    # ax.set_xticks(xticks, crs=ccrs.PlateCarree())
    # ax.set_yticks(yticks, crs=ccrs.PlateCarree())
    # ax.xaxis.set_major_formatter(LongitudeFormatter())
    # ax.xaxis.set_minor_locator(plt.MultipleLocator(2.5))
    # ax.yaxis.set_major_formatter(LatitudeFormatter())
    # ax.yaxis.set_minor_locator(plt.MultipleLocator(2.5))

    # --设置刻度
    ax.tick_params(
        axis='both', 
        which='major',
        direction='out',
        length=3.5,
        width=0.8,
        color='k',
        pad=1,
        labelsize=10,
        labelcolor='k',
        top=False,
        right=False,
        labeltop=False,
        labelright=False,
        )
    ax.tick_params(
        axis='both', 
        which='minor',
        direction='out',
        length=2.5,
        width=0.8,
        color='k',
        pad=1,
        labelsize=10,
        labelcolor='k',
        top=False,
        right=False,
        labeltop=False,
        labelright=False,
        )
    
    return ax

# 创建Map类坐标轴-东亚地区 AlbersEqualArea
def create_map_AlbersEqualArea(fig,scale=None,shp_file=None):
    # --创建地图投影
    proj = ccrs.AlbersEqualArea(central_longitude=104.7, central_latitude=0.0, false_easting=0.0, false_northing=0.0, standard_parallels=(20.0, 50.0),)
    # proj = ccrs.LambertConformal(central_longitude=105, standard_parallels=(25, 47))
    # --创建画图空间
    ax = fig.add_subplot(1,1, 1, projection=proj)
    # -- 设置范围
    ax.set_extent([70, 140, 10, 50], crs=ccrs.PlateCarree())
    
    # --设置地图信息
    if scale is None:
        scale = 50
    if shp_file is not None:
        provinces = cfeat.ShapelyFeature(
            Reader(shp_file).geometries(),
            proj, edgecolor='k',
            facecolor='none'
        )
        # 加载省界线
        ax.add_feature(provinces, linewidth=0.6, zorder=2)
    ## 加载分辨率为50的海岸线
    ax.add_feature(cfeat.COASTLINE.with_scale(f'{scale}m'), linewidth=0.6, zorder=10)
    ## 加载分辨率为50的河流~
    ax.add_feature(cfeat.RIVERS.with_scale(f'{scale}m'), zorder=10)
    ## 加载分辨率为50的湖泊~
    ax.add_feature(cfeat.LAKES.with_scale(f'{scale}m'), zorder=10)
    
    # *must* call draw in order to get the axis boundary used to add ticks:
    fig.canvas.draw()
    xticks=(np.arange(75, 135 + 5, 5)).tolist()
    yticks=(np.arange(10, 40 + 5, 5)).tolist()


    # --设置网格属性 Grindline+NCL风格
    gl = ax.gridlines(
        xlocs=xticks, 
        ylocs=yticks, 
        draw_labels = False,
        linewidth = 0.7,
        color = 'grey',
        alpha = 0.5,
        linestyle = '--',
    )
    #使横坐标转化为经纬度格式
    # Label the end-points of the gridlines using the custom tick makers:
    # # --设置经纬度（1）经纬度是正的
    # ax.xaxis.set_major_formatter(LONGITUDE_FORMATTER) 
    # ax.yaxis.set_major_formatter(LATITUDE_FORMATTER)
    # lambert_xticks(ax, xticks)
    # lambert_yticks(ax, yticks)

    # --设置经纬度（2）经纬度是斜着的
    gl.top_labels=gl.right_labels=False
    gl.bottom_labels=gl.left_labels=True
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    gl.xlocator=mticker.FixedLocator(np.arange(75, 135 + 5, 5))
    gl.ylocator=mticker.FixedLocator(np.arange(10, 40 + 5, 5))
    
    # --设置经纬度（3）只适用于等经纬度 因此不适用该投影
    # ax.set_xticks(np.arange(75, 15 + 5, 5), crs=ccrs.PlateCarree())
    # ax.set_yticks(np.arange(10, 40 + 5, 5), crs=ccrs.PlateCarree())
    # ax.xaxis.set_major_formatter(LongitudeFormatter())
    # ax.xaxis.set_minor_locator(plt.MultipleLocator(2.5))
    # ax.yaxis.set_major_formatter(LatitudeFormatter())
    # ax.yaxis.set_minor_locator(plt.MultipleLocator(2.5))
    
    
    # --设置刻度
    ax.tick_params(
        axis='both', 
        which='major',
        direction='out',
        length=3.5,
        width=0.8,
        color='k',
        pad=1,
        labelsize=10,
        labelcolor='k',
        top=False,
        right=False,
        labeltop=False,
        labelright=False,
        )
    ax.tick_params(
        axis='both', 
        which='minor',
        direction='out',
        length=2.5,
        width=0.8,
        color='k',
        pad=1,
        labelsize=10,
        labelcolor='k',
        top=False,
        right=False,
        labeltop=False,
        labelright=False,
        )
    return ax

# 创建Map类坐标轴-全球地区 PlateCarree
def create_map_PlateCarree_Globe(fig,scale=110,shp_file=None):
    # --创建地图投影
    proj = ccrs.PlateCarree(central_longitude=0)
    
    # --创建画图空间
    ax = fig.add_subplot(1,1, 1, projection=proj)

    # -- 设置范围
    ax.set_extent([-180, 180, -90, 90], crs=ccrs.PlateCarree())
    
    # --设置地图信息
    ## 加载分辨率为scale的海岸线
    ax.add_feature(cfeat.COASTLINE.with_scale(f'{scale}m'), linewidth=0.6, zorder=10)
    ## 加载分辨率为scale的河流~
    ax.add_feature(cfeat.RIVERS.with_scale(f'{scale}m'), zorder=10)
    ## 加载分辨率为scale的湖泊~
    ax.add_feature(cfeat.LAKES.with_scale(f'{scale}m'), zorder=10)

    # --设置网格属性 Grindline+NCL风格
    gl = ax.gridlines(
        draw_labels = False,
        linewidth = 0.7,
        color = 'grey',
        alpha = 0.5,
        linestyle = '--'
    )
    gl.top_labels = gl.right_labels = gl.left_labels = gl.bottom_labels = False  # 关闭经纬度标签

    # --设置经纬度（3）
    ax.set_xticks(np.arange(-180, 180 + 60, 60))
    ax.set_yticks(np.arange(-90, 90 + 30, 30))
    ax.xaxis.set_major_formatter(LongitudeFormatter())
    ax.xaxis.set_minor_locator(plt.MultipleLocator(20))
    ax.yaxis.set_major_formatter(LatitudeFormatter())
    ax.yaxis.set_minor_locator(plt.MultipleLocator(15))
    
    # --设置刻度
    ax.tick_params(
        axis='both', 
        which='major',
        direction='out',
        length=3.5,
        width=0.8,
        color='k',
        pad=1,
        labelsize=10,
        labelcolor='k',
        top=False,
        right=False,
        labeltop=False,
        labelright=False,
        )
    ax.tick_params(
        axis='both', 
        which='minor',
        direction='out',
        length=2.5,
        width=0.8,
        color='k',
        pad=1,
        labelsize=10,
        labelcolor='k',
        top=False,
        right=False,
        labeltop=False,
        labelright=False,
        )
    return ax

# 创建Map类坐标轴-南极地区 SouthPole
def create_map_PlateCarree_South(fig,scale=50,shp_file=None,boundary=True):
    # --创建地图投影
    proj = ccrs.SouthPolarStereo(central_longitude=0)
    
    # --创建画图空间
    ax = fig.add_subplot(1,1, 1, projection=proj)

    # -- 设置范围
    ax.set_extent([-180, 180, -90, -60], ccrs.PlateCarree())
    theta = np.linspace(0, 2*np.pi, 100)
    center, radius = [0.5, 0.5], 0.5
    verts = np.vstack([np.sin(theta), np.cos(theta)]).T
    circle = mpath.Path(verts * radius + center)
    
    # --设置地图信息
    ## 加载分辨率为scale的海岸线
    ax.add_feature(cfeat.COASTLINE.with_scale(f'{scale}m'), linewidth=0.6, zorder=10)
    ## 加载分辨率为scale的河流~
    ax.add_feature(cfeat.RIVERS.with_scale(f'{scale}m'), zorder=10)
    ## 加载分辨率为scale的湖泊~
    ax.add_feature(cfeat.LAKES.with_scale(f'{scale}m'), zorder=10)
    
    # *must* call draw in order to get the axis boundary used to add ticks:
    fig.canvas.draw()
    xticks=(np.arange(-180, 180 + 60, 60)).tolist()
    yticks=(np.arange(-90, 90 + 30, 30)).tolist()

    # --设置网格属性 Grindline+NCL风格
    gl = ax.gridlines(
        xlocs = xticks,
        ylocs = yticks,
        crs = ccrs.PlateCarree(),
        draw_labels = True,
        linewidth = 0.7,
        color = 'grey',
        alpha = 0.5,
        linestyle = '--'
    )
    gl.top_labels = gl.right_labels = gl.left_labels = gl.bottom_labels = True  # 关闭经纬度标签
    # Label the end-points of the gridlines using the custom tick makers:
    if boundary:
        ax.set_boundary(circle, transform=ax.transAxes)
    return ax

# 创建单独的colorbar
def create_colorbar(fig,cmap,bounds,title=None):
    cax = fig.add_axes([.06, .045, 0.87, .03])
    norm = mpl.colors.Normalize(vmin=bounds[0], vmax=bounds[-1])
    cb3 = mpl.colorbar.ColorbarBase(ax=cax,
                                    cmap=cmap,
                                    norm=norm,
                                    # to use 'extend', you must
                                    # specify two extra boundaries:
                                    boundaries= bounds,
                                    extend='both',
                                    ticks=bounds[::3],  # optional
                                    spacing='proportional',
                                    orientation='horizontal',#'vertical',
                                    # label='Temperature',
                                    )
    if title is None:
        cax.set_title('Temperature')
    else:
        cax.set_title(title)
    cax.tick_params(labelsize=12,direction='in')

# 创建折线图
def create_plot_datetime(fig,labels,positions):
    # 中文设置为宋体 英文为极为接近的Times new roman
    config = {
                "font.family": 'serif',
                "font.size": 10,
                "mathtext.fontset": 'stix',
                "font.serif": ['SimSun'],
            }
    plt.rcParams.update(config)
    # --创建画图空间
    ax = fig.add_subplot(1,1, 1)
    fontsize = 12
    ax.xaxis.set_major_formatter(mdate.DateFormatter('%Y-%m-%d'))
    ymajorFormatter = mticker.FormatStrFormatter('%.1f')  # 设置y轴标签文本的格式
    ymajorLocator = mticker.MultipleLocator(0.4)  # 将y轴主刻度标签设置为50的倍数
    yminorLocator = mticker.MultipleLocator(0.1)  # 将y轴次刻度标签设置为50的倍数
    ax.yaxis.set_major_formatter(ymajorFormatter)
    ax.yaxis.set_major_locator(ymajorLocator)
    ax.yaxis.set_minor_locator(yminorLocator)


    xmajorFormatter = mticker.FormatStrFormatter('%s')  # 设置x轴标签文本的格式
    xmajorLocator = mticker.MultipleLocator(10)         # 将x轴主刻度标签设置为5的倍数
    xminorLocator = mticker.MultipleLocator(2)
    # ax.xaxis.set_major_formatter(xmajorFormatter)
    # ax.xaxis.set_major_locator(xmajorLocator)
    ax.xaxis.set_major_locator(mticker.FixedLocator(positions))
    ax.xaxis.set_major_formatter(mticker.FixedFormatter(labels))
    ax.xaxis.set_minor_locator(xminorLocator)

    ax.tick_params(top=True, right=True, labeltop=False, labelright=False)
    ax.tick_params(axis='y', which='major', color='k', labelcolor='k', labelsize=fontsize, width=1.2, direction='in',length=6)
    ax.tick_params(axis='y', which='minor', color='k', labelcolor='k', labelsize=fontsize, width=1.2, direction='in',length=4)
    ax.tick_params(axis='x', which='major', color='k', labelcolor='k', labelsize=fontsize, width=1.2, direction='in',length=6)
    ax.tick_params(axis='x', which='minor', color='k', labelcolor='k', labelsize=fontsize, width=1.2, direction='in',length=4)
    
    ax.set_xlim(0,58)
    ax.set_ylim(-1,1)

    ax.set_title('宋体')

    return ax