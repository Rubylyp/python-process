dset NCEP_slp_30y_Wt.dat
undef  1.0E+33
title  NCEP data 
xdef  48  levels   5          12.5            20          27.5            35          42.5            50          57.5            65          72.5            80          87.5            95         102.5           110         117.5           125         132.5           140         147.5           155         162.5           170         177.5           185         192.5           200         207.5           215         222.5           230         237.5           245         252.5           260         267.5           275         282.5           290         297.5           305         312.5           320         327.5           335         342.5           350        356.25
ydef  24  levels   -87.5          -80        -72.5          -65        -57.5          -50        -42.5          -35        -27.5          -20        -12.5           -5          2.5           10         17.5           25         32.5           40         47.5           55         62.5           70         77.5           85
zdef   1  levels   1000
tdef  30  linear  0z01Jan1978  1yr
vars   1
slp   0   99  SLP Winter
endvars
