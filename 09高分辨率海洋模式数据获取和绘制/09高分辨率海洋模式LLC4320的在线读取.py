import xmitgcm.llcreader as llcreader
# %matplotlib inline
import holoviews as hv
from holoviews.operation.datashader import regrid
hv.extension('bokeh')

model = llcreader.ECCOPortalLLC4320Model()
print(model)

# volecity
ds = model.get_dataset(varnames=['U', 'V'], k_levels=[0], type='latlon',
                        iter_start=model.iter_start,
                        iter_stop=(model.iter_start + model.iter_step),
                        read_grid=True)

# Normal gridding
import xgcm
grid = xgcm.Grid(ds, periodic=['X'])

# Calculate vorticity
zeta = (-grid.diff(ds.U * ds.dxC, 'Y', boundary='fill') + grid.diff(ds.V * ds.dyC, 'X'))/ds.rAz
zeta = zeta.squeeze().rename('vorticity').reset_coords(drop=True)

# load data
zeta.load()

# Show
dataset = hv.Dataset(zeta)
hv_im = (dataset.to(hv.Image, ['i_g', 'j_g'])
                .options(cmap='RdBu_r', width=950, height=600, colorbar=True, symmetric=True))

regrid(hv_im, precompute=True)